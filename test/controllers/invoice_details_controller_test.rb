require 'test_helper'

class InvoiceDetailsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @invoice_detail = invoice_details(:one)
  end

  test "should get index" do
    get invoice_details_url, as: :json
    assert_response :success
  end

  test "should create invoice_detail" do
    assert_difference('InvoiceDetail.count') do
      post invoice_details_url, params: { invoice_detail: { description: @invoice_detail.description, invoice_id: @invoice_detail.invoice_id, subtotal: @invoice_detail.subtotal } }, as: :json
    end

    assert_response 201
  end

  test "should show invoice_detail" do
    get invoice_detail_url(@invoice_detail), as: :json
    assert_response :success
  end

  test "should update invoice_detail" do
    patch invoice_detail_url(@invoice_detail), params: { invoice_detail: { description: @invoice_detail.description, invoice_id: @invoice_detail.invoice_id, subtotal: @invoice_detail.subtotal } }, as: :json
    assert_response 200
  end

  test "should destroy invoice_detail" do
    assert_difference('InvoiceDetail.count', -1) do
      delete invoice_detail_url(@invoice_detail), as: :json
    end

    assert_response 204
  end
end
