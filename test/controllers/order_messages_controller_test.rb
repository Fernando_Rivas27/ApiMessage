require 'test_helper'

class OrderMessagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @order_message = order_messages(:one)
  end

  test "should get index" do
    get order_messages_url, as: :json
    assert_response :success
  end

  test "should create order_message" do
    assert_difference('OrderMessage.count') do
      post order_messages_url, params: { order_message: { message: @order_message.message, order_id: @order_message.order_id, route: @order_message.route } }, as: :json
    end

    assert_response 201
  end

  test "should show order_message" do
    get order_message_url(@order_message), as: :json
    assert_response :success
  end

  test "should update order_message" do
    patch order_message_url(@order_message), params: { order_message: { message: @order_message.message, order_id: @order_message.order_id, route: @order_message.route } }, as: :json
    assert_response 200
  end

  test "should destroy order_message" do
    assert_difference('OrderMessage.count', -1) do
      delete order_message_url(@order_message), as: :json
    end

    assert_response 204
  end
end
