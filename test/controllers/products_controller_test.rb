require 'test_helper'

class ProductsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @product = products(:one)
  end

  test "should get index" do
    get products_url, as: :json
    assert_response :success
  end

  test "should create product" do
    assert_difference('Product.count') do
      post products_url, params: { product: { active: @product.active, available: @product.available, category_id: @product.category_id, description: @product.description, image_content_type: @product.image_content_type, image_file_name: @product.image_file_name, image_file_size: @product.image_file_size, image_updated_at: @product.image_updated_at, measure_id: @product.measure_id, new: @product.new, participant_id: @product.participant_id, price: @product.price, product_catalog_id: @product.product_catalog_id, product_id: @product.product_id, product_name: @product.product_name, score: @product.score, stock: @product.stock } }, as: :json
    end

    assert_response 201
  end

  test "should show product" do
    get product_url(@product), as: :json
    assert_response :success
  end

  test "should update product" do
    patch product_url(@product), params: { product: { active: @product.active, available: @product.available, category_id: @product.category_id, description: @product.description, image_content_type: @product.image_content_type, image_file_name: @product.image_file_name, image_file_size: @product.image_file_size, image_updated_at: @product.image_updated_at, measure_id: @product.measure_id, new: @product.new, participant_id: @product.participant_id, price: @product.price, product_catalog_id: @product.product_catalog_id, product_id: @product.product_id, product_name: @product.product_name, score: @product.score, stock: @product.stock } }, as: :json
    assert_response 200
  end

  test "should destroy product" do
    assert_difference('Product.count', -1) do
      delete product_url(@product), as: :json
    end

    assert_response 204
  end
end
