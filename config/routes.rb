Rails.application.routes.draw do
  #resources :roles
  #resources :products
  #resources :product_catalogs
  #resources :participants
  #resources :orders
  resources :order_messages
  #resources :order_details
  #resources :measures
  #resources :invoices
  #resources :invoice_details
  #resources :categories

  root 'order_messages#index' #ruta principal de la api
  get 'order_message_suppliers_received/:id',to:'order_messages#menssage_id_supplier_received'
  get 'order_message_client_received/:id',to:'order_messages#menssage_id_client_received'
  get 'order_message_suppliers_send/:id',to:'order_messages#menssage_id_supplier_send'
  get 'order_message_client_send/:id',to:'order_messages#menssage_id_client_send'
  get 'order_message_order/:id',to:'order_messages#menssage_order'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
