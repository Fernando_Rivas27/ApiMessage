# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170425052434) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "btree_gin"
  enable_extension "btree_gist"
  enable_extension "citext"
  enable_extension "cube"
  enable_extension "dblink"
  enable_extension "dict_int"
  enable_extension "dict_xsyn"
  enable_extension "earthdistance"
  enable_extension "fuzzystrmatch"
  enable_extension "hstore"
  enable_extension "intarray"
  enable_extension "ltree"
  enable_extension "pg_stat_statements"
  enable_extension "pg_trgm"
  enable_extension "pgcrypto"
  enable_extension "pgrowlocks"
  enable_extension "pgstattuple"
  enable_extension "tablefunc"
  enable_extension "unaccent"
  enable_extension "uuid-ossp"
  enable_extension "xml2"

  create_table "categories", force: :cascade do |t|
    t.string   "category_name", limit: 20
    t.string   "description",   limit: 40
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "invoice_details", force: :cascade do |t|
    t.integer  "invoice_id"
    t.string   "description", limit: 100
    t.decimal  "subtotal",                precision: 8, scale: 2
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.index ["invoice_id"], name: "index_invoice_details_on_invoice_id", using: :btree
  end

  create_table "invoices", force: :cascade do |t|
    t.integer  "order_id"
    t.string   "code",         limit: 14
    t.datetime "date_invoice"
    t.string   "nit",          limit: 14
    t.decimal  "amount",                  precision: 8, scale: 2
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.index ["order_id"], name: "index_invoices_on_order_id", using: :btree
  end

  create_table "measures", force: :cascade do |t|
    t.string   "measure_name", limit: 20
    t.string   "abbreviation", limit: 3
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "order_details", force: :cascade do |t|
    t.integer  "order_id"
    t.integer  "product_id"
    t.decimal  "quantity",              precision: 6, scale: 2
    t.decimal  "sale_price",            precision: 8, scale: 2
    t.decimal  "amount",                precision: 8, scale: 2
    t.decimal  "score",                 precision: 5, scale: 2
    t.string   "commentary", limit: 50
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.index ["order_id"], name: "index_order_details_on_order_id", using: :btree
    t.index ["product_id"], name: "index_order_details_on_product_id", using: :btree
  end

  create_table "order_messages", force: :cascade do |t|
    t.integer  "order_id"
    t.string   "message",    limit: 140
    t.boolean  "route"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["order_id"], name: "index_order_messages_on_order_id", using: :btree
  end

  create_table "orders", force: :cascade do |t|
    t.integer  "supplier_id"
    t.integer  "client_id"
    t.datetime "date_delivery"
    t.decimal  "amount",                   precision: 8, scale: 2
    t.string   "status",        limit: 20
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
  end

  create_table "participants", force: :cascade do |t|
    t.integer  "role_id"
    t.string   "email",      limit: 50
    t.string   "first_name", limit: 20
    t.string   "last_name",  limit: 20
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.index ["role_id"], name: "index_participants_on_role_id", using: :btree
  end

  create_table "product_catalogs", force: :cascade do |t|
    t.string   "catalog_name", limit: 20
    t.string   "description",  limit: 40
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "products", force: :cascade do |t|
    t.integer  "category_id"
    t.integer  "measure_id"
    t.integer  "product_catalog_id"
    t.integer  "participant_id"
    t.integer  "product_id"
    t.string   "product_name",       limit: 20
    t.string   "description",        limit: 40
    t.decimal  "price",                         precision: 8, scale: 2
    t.decimal  "stock",                         precision: 8, scale: 2
    t.decimal  "score",                         precision: 5, scale: 2
    t.boolean  "new"
    t.boolean  "available"
    t.boolean  "active"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at",                                            null: false
    t.datetime "updated_at",                                            null: false
    t.index ["category_id"], name: "index_products_on_category_id", using: :btree
    t.index ["measure_id"], name: "index_products_on_measure_id", using: :btree
    t.index ["participant_id"], name: "index_products_on_participant_id", using: :btree
    t.index ["product_catalog_id"], name: "index_products_on_product_catalog_id", using: :btree
    t.index ["product_id"], name: "index_products_on_product_id", using: :btree
  end

  create_table "roles", force: :cascade do |t|
    t.string   "rol_name",   limit: 20
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_foreign_key "invoice_details", "invoices"
  add_foreign_key "invoices", "orders"
  add_foreign_key "order_details", "orders"
  add_foreign_key "order_details", "products"
  add_foreign_key "order_messages", "orders"
  add_foreign_key "orders", "participants", column: "client_id", name: "cliefk"
  add_foreign_key "orders", "participants", column: "supplier_id", name: "suppfk"
  add_foreign_key "participants", "roles"
  add_foreign_key "products", "categories"
  add_foreign_key "products", "measures"
  add_foreign_key "products", "participants"
  add_foreign_key "products", "product_catalogs"
  add_foreign_key "products", "products"
end
