class ProductCatalogsController < ApplicationController
  before_action :set_product_catalog, only: [:show, :update, :destroy]

  # GET /product_catalogs
  def index
    @product_catalogs = ProductCatalog.all

    render json: @product_catalogs
  end

  # GET /product_catalogs/1
  def show
    render json: @product_catalog
  end

  # POST /product_catalogs
  def create
    @product_catalog = ProductCatalog.new(product_catalog_params)

    if @product_catalog.save
      render json: @product_catalog, status: :created, location: @product_catalog
    else
      render json: @product_catalog.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /product_catalogs/1
  def update
    if @product_catalog.update(product_catalog_params)
      render json: @product_catalog
    else
      render json: @product_catalog.errors, status: :unprocessable_entity
    end
  end

  # DELETE /product_catalogs/1
  def destroy
    @product_catalog.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product_catalog
      @product_catalog = ProductCatalog.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def product_catalog_params
      params.require(:product_catalog).permit(:catalog_name, :description)
    end
end
