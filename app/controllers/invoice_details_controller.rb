class InvoiceDetailsController < ApplicationController
  before_action :set_invoice_detail, only: [:show, :update, :destroy]

  # GET /invoice_details
  def index
    @invoice_details = InvoiceDetail.all

    render json: @invoice_details
  end

  # GET /invoice_details/1
  def show
    render json: @invoice_detail
  end

  # POST /invoice_details
  def create
    @invoice_detail = InvoiceDetail.new(invoice_detail_params)

    if @invoice_detail.save
      render json: @invoice_detail, status: :created, location: @invoice_detail
    else
      render json: @invoice_detail.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /invoice_details/1
  def update
    if @invoice_detail.update(invoice_detail_params)
      render json: @invoice_detail
    else
      render json: @invoice_detail.errors, status: :unprocessable_entity
    end
  end

  # DELETE /invoice_details/1
  def destroy
    @invoice_detail.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_invoice_detail
      @invoice_detail = InvoiceDetail.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def invoice_detail_params
      params.require(:invoice_detail).permit(:invoice_id, :description, :subtotal)
    end
end
