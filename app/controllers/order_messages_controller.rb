class OrderMessagesController < ApplicationController
  before_action :set_order_message, only: [:show, :update, :destroy]
  #before_action :authenticate_request
def menssage_id_supplier_send() #metodo para mensajes enviados por proveedor

@order_messages= OrderMessage.joins(:order).where('orders.supplier_id'=>params[:id]).where('order_messages.route'=> 'true')

render json:@order_messages

end


def menssage_id_client_send() # metodo por mensajes enviados por clientes

@order_messages= OrderMessage.joins(:order).where('orders.client_id'=>params[:id] ).where('order_messages.route'=> 'false')

render json:@order_messages

end

def menssage_id_supplier_received() #metodo para mensajes recividos por proveedor

@order_messages= OrderMessage.joins(:order).where('orders.supplier_id'=>params[:id]).where('order_messages.route'=> 'false')

render json:@order_messages

end


def menssage_id_client_received() #metodo para mesajes recibidos por clientes

@order_messages= OrderMessage.joins(:order).where('orders.client_id'=>params[:id] ).where('order_messages.route'=> 'true')

render json:@order_messages

end



def menssage_order() # metodo para obtener los mensajes de una orden especifica

@order_messages= OrderMessage.joins(:order).where('orders.id'=>params[:id])

render json:@order_messages

end
  # GET /order_messages
  def index  # metodo para una consulta de todos los mensajes
    @order_messages = OrderMessage.all

    render json: @order_messages
  end

  # GET /order_messages/1
  def show    # metodo para mostrar un mesaje especifico
    render json: @order_message
  end

  # POST /order_messages
  def create  # metodo para crear un mensaje
    @order_message = OrderMessage.new(order_message_params)

    if @order_message.save
      render json: @order_message, status: :created, location: @order_message
    else
      render json: @order_message.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /order_messages/1
  def update # metodo para actualizar un mensaje
    if @order_message.update(order_message_params)
      render json: @order_message
    else
      render json: @order_message.errors, status: :unprocessable_entity
    end
  end

  # DELETE /order_messages/1
  def destroy  # metodo para eliminar un mensaje
    @order_message.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order_message
      @order_message = OrderMessage.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def order_message_params
      params.permit(:order_id, :message, :route)
    end
end
