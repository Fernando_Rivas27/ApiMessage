class ApplicationController < ActionController::API
  require 'json'

#attr_reader :current_user

private

def  authenticate_request
   if request.headers['Authorization'].present?
    @token = request.headers['Authorization'].split(' ').last
    @current_user = auth_api(@token)
    render json: {error: 'Not Authorized'}, status: 401 unless @current_user
   else
     render json: {error: 'No token in headers'}
   end
 end

 def auth_api(token)
       response = Typhoeus.get("http://pseesapiauth.herokuapp.com/check?token="+token)

       user = JSON.parse(response.body)['user']
       if user
         return user
       else return nil
       end


  end
end
