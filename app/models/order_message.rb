class OrderMessage < ApplicationRecord
  belongs_to :order


validates :order_id, presence: true
validates :message , presence: true
validates :message,length:{ in: 10..500}

validates :order_id, numericality: { only_integer: true }
validates :route, inclusion: {in:[true,false]}
validates :route, exclusion: {in:[nil]}
end
