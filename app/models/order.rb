class Order < ApplicationRecord
  belongs_to :supplier
  belongs_to :client
  has_many :order_message
end
