class Product < ApplicationRecord
  belongs_to :category
  belongs_to :measure
  belongs_to :product_catalog
  belongs_to :participant
  belongs_to :product
end
