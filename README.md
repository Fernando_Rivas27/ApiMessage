
Service Specification

https://apimessage.herokuapp.com/

This service manages the Entity:

Order Messages
The json formats used to indicate the "Recieves" and "Responses"

Routes Dictionary:

- post /order_messages
create a new order messages ,Returning the new message with all your data.


Recieves:

{"order_id": "1","message": "Hola recibi tu solicitud","route": "true/false"}

Responses:

{
"id":20,
"order_id":66,
"message":"Hola recibi tu pedido, cualquier consulta estoy a tu servicio.",
"route":true,
"created_at":"2017-05-07T05:52:46.035Z",
"updated_at":"2017-05-07T05:52:46.035Z"
}

-get/order_messages

Return all messages

Responses:

{"id":20,"order_id":66,"message":"Hola recibi tu pedido, cualquier consulta estoy a tu servicio.","route":true}



-get/order_message_order/:id
Returns all messages of an order according to their id

Responses:

{"id":17,"order_id":23,"message":"prueba desde potman v1","route":true}
rder_message_suppliers_received/:id





-get/order_message_suppliers_received/:id

Returns all messages received from a specific provider

Responses:

{"id":8,"order_id":44,"message":"Hola recibi tu pedido, cualquier consulta estoy a tu servicio.","route":false}


-get/order_message_client_received/:id
Returns all messages received from a specific client

Responses:

{"id":6,"order_id":42,"message":"Hola recibi tu pedido, cualquier consulta estoy a tu servicio.","route":true}

-get//order_message_suppliers_send/:id

Returns all messages sent from a provider

Responses:

{"id":6,"order_id":42,"message":"Hola recibi tu pedido, cualquier consulta estoy a tu servicio.","route":true}

-get/order_message_client_send/:id

Returns all messages sent from a client

Responses:

{"id":8,"order_id":44,"message":"Hola recibi tu pedido, cualquier consulta estoy a tu servicio.","route":false}

patch/order_messages/:id

Update the fields of a message as desired

Recieves:

{"order_id": "3","message": "Hola recibi tu solicitud esta en camino","route": "true"}

Responses:

{
"id":1,
"order_id":3,
"message": "Hola recibi tu solicitud esta en camino",
"route":true,
"created_at":"2017-05-07T05:52:46.035Z",
"updated_at":"2017-05-07T05:52:46.035Z"
}




put/order_messages/:id

Update the fields of a message as desired

Recieves:

{"message": "Hola recibi tu solicitud esta en camino"}
Responses:

{
"id":20,
"order_id":66,
"message": "Hola recibi tu solicitud esta en camino",
"route":true,
"created_at":"2017-05-07T05:52:46.035Z",
"updated_at":"2017-05-07T05:52:46.035Z"
}



delete/order_messages/:id
Delete selected message

Responses:

Deletes a order message
